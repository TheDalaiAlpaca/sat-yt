# sat-yt

YouTube player based on saturnon, yt-dlp and mpv

## Features

- Play video
- Subscribe to channel
- Search videos
- View video info
- Move channel to another directory
- Download video's

## Installation

This package is available as [sat-yt-git](https://aur.archlinux.org/packages/sat-yt-git/) in the AUR.

## How to use

Requirements:  

- saturnon
- mpv
- yt-dlp
- jq

Run:  

`saturnon -c /etc/saturnon/youtube.conf`

You will end up in your new YouTube database directory `$HOME/Videos/sat-youtube`. To search, enter either the search_populare or the search_recent directory and press space. You will be prompted for a search term. Now type some keyword and press enter. After a few seconds, a list of 20 results should appear. These are actually files with video URLs in them. Press enter to stream a video with mpv. Press 'i' to view details about the video and 's' to subscribe to the uploader of the video. A channel directory of the uploaders name will appear parallel to the search directory. Feel free to organize channel directories into subdirectories. Press space in a channel directory to load the 20 most recently uploaded videos. You can change this number by changing the value of `maxload` in the `.chaninfo` file that is found in each channel directory.  

For a full list of keybinds, press `?`.
